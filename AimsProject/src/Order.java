
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	int qtyOrdered = 0;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemsOrdered[qtyOrdered] = disc;
			qtyOrdered++;
			System.out.println("The disc has been added.");
		}
		else {
			System.out.println("The order is almost full.");
			System.exit(0);
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int i = 0;
		for (i = 0; i < qtyOrdered; i++) {
			if(itemsOrdered[i] == disc) {
				if(i == qtyOrdered -1){
					itemsOrdered[i] = null;
					qtyOrdered--;
					System.out.println("The disc name "+ disc.getTitle()+ " has removed.");
					return;
				}
				for(int j = i; j < qtyOrdered; j++) {
					itemsOrdered[j] = itemsOrdered[j+1];
				}
				break;
			}
		}
		i++;
		if(i > qtyOrdered) {
			System.out.println("Cann't find the disc that you want to remove.");
		}
		else {
			itemsOrdered[i-1] = null;
			qtyOrdered--;
			System.out.println("The disc name "+ disc.getTitle()+ " has removed.");
		}
		
	}
	
	public float totalCost(){
		int i = 0;
		float total = 0, cost;
		for(i = 0; i < qtyOrdered; i++) {
			cost = itemsOrdered[i].getCost();
			total += cost;
		}
		return total;
	}
}